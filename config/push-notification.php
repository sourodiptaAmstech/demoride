<?php

return array(

    'IOSUser'     => array(
        'environment' => 'production',
        'certificate' => app_path().'/apns/user/ride-share-demo-passenger-production-no-passphrase.pem',
        //'environment' => 'development',
        //'certificate' => app_path().'/apns/user/ride-share-demo-passenger-development-no-passphrase.pem',
        'passPhrase'  => '',
        'service'     => 'apns'
    ),
    'IOSProvider' => array(
        //'environment' => 'development',
        //'certificate' => app_path().'/apns/provider/ride-share-demo-driver-development-no-passphrase.pem',
        'environment' => 'production',
        'certificate' => app_path().'/apns/provider/ride-share-demo-driver-production-no-passphrase.pem',
       'passPhrase'  => '',
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' => 'development',
        'apiKey'      => env('ANDROID_USER_PUSH_KEY'),
        'service'     => 'gcm'
    ),
    'AndroidProvider' => array(
        'environment' => 'development',
        'apiKey'      => env('ANDROID_PROVIDER_PUSH_KEY'),
        'service'     => 'gcm'
    )

);