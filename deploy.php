<?php
namespace Deployer;

require 'recipe/laravel.php';
require 'vendor/deployer/recipes/recipe/npm.php';

// Configuration

set('repository', 'bitbucket.org:uturn2018A/uturn_web.git');
//set('git_tty', false); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);
set('allow_anonymous_stats', false);

set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('writable_mode', 'chmod');
set('local_deploy_path', '/tmp/deployer');

// Hosts

host('uturnride.com')
    ->port(4422)
    ->user('root')
    ->identityFile('~/.ssh/id_rsa')
    ->set('deploy_path', '/var/www/html');

// Tasks 
 
task('test', function () {
    $result = run('pwd');
    writeln($result);
});

task('npm:build', function () {
    run("cd {{release_path}} && {{bin/npm}} run prod");
});

task('storagelink',function(){
    run("cd {{release_path}}/public && ln -s ../../../shared/storage/app/public storage");
});

task('uploadslink',function(){
    run("cd {{release_path}}/public && ln -s ../../../shared/uploads uploads");
});

task('master:prepare',function() {
    run('deployer deploy:prepare');
});

task('master:release',function() {
    run('deployer deploy:release');
});

task('master:update_code',function() {
    run('deployer deploy:update_code');
});

task('master:shared',function() {
    run('deployer deploy:shared');
});

task('master:vendors',function() {
    run('deployer deploy:vendors');
});

task('master:migrate',function() {
    run('deployer artisan:migrate');
});

task('master:install',function() {
    run('deployer npm:install');
});

task('master:build',function() {
    run('deployer npm:build');
});

task('master:storagelink',function() {
    run('deployer storagelink');
});

task('master:symlink',function() {
    run('deployer deploy:symlink');
});

task('master:cleanup',function() {
    run('deployer cleanup');
});

task('beta:prepare',function() {
    run('deployer -f=./deploy-dev.php deploy:prepare');
});

task('beta:release',function() {
    run('deployer -f=./deploy-dev.php deploy:release');
});

task('beta:update_code',function() {
    run('deployer -f=./deploy-dev.php deploy:update_code');
});

task('beta:shared',function() {
    run('deployer -f=./deploy-dev.php deploy:shared');
});

task('beta:vendors',function() {
    run('deployer -f=./deploy-dev.php deploy:vendors');
});

task('beta:migrate',function() {
    run('deployer -f=./deploy-dev.php artisan:migrate');
});

task('beta:install',function() {
    run('deployer -f=./deploy-dev.php npm:install');
});

task('beta:build',function() {
    run('deployer -f=./deploy-dev.php npm:build');
});

task('beta:storagelink',function() {
    run('deployer -f=./deploy-dev.php storagelink');
});

task('beta:symlink',function() {
    run('deployer -f=./deploy-dev.php deploy:symlink');
});

task('beta:cleanup',function() {
    run('deployer -f=./deploy-dev.php cleanup');
});

task('start', [
    'deploy:prepare',       // Create dirs
    'deploy:release',       // Release number
    'deploy:update_code',   // git clone
    'deploy:shared',        // Shared and .env linking on server
    'deploy:vendors',       // composer install locally
    'artisan:migrate',      // Migrate DB on server
    'npm:install',          // NPM install
    'npm:build',            // NPM production build
    'storagelink',          // Symlink in public to storage
    'deploy:symlink',       // Symlink /current on server
    'cleanup',              // Cleanup old releases on server
]);