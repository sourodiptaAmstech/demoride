<?php

namespace App\Http\Controllers\Resource;

use Auth;
use Setting;
use Exception;

use App\Provider;
use Carbon\Carbon;
use App\Promocode;
use App\PromotionUsages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PromotionsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $promotions = Promocode::orderBy('created_at' , 'desc')->where('user_type', 'DRIVER')->get();
        
        if($request->has('app')) {
            return response()->json([
                'promotions' => $promotions,
            ]);
        } else {
            return view('admin.promotions.index', compact('promotions'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        $this->validate($request, [
            'content' => 'required',
            'discount' => 'required|numeric',
            'expiration' => 'required',
            'driver_discout_criteria_time' => 'integer|min:0',
            'driver_discout_criteria_distance' => 'integer|min:0',
            'activated_fordriver' => 'integer|in:0,1'
        ]);

        try{
            //Request::merge(['user_type' => 'DRIVER']);
            //$request->merge('user_type', 'DRIVER');
            //$request->attributes->add(["user_type" => 'DRIVER']);

            $promotions = Promocode::where('user_type', 'DRIVER')->get();
            if($request->activated_fordriver == '1'){
                foreach ($promotions as $promotion) {
                    if($promotion->activated_fordriver == '1'){
                        return redirect('admin/promotion')->with('flash_error','One Promotion is already been activated');
                    }
                }
            }
            $arr = $request->all();
            $arr["user_type"] = 'DRIVER';

            Promocode::create($arr);
            return redirect('admin/promotion')->with('flash_success','Promotion Saved Successfully');
        } 

        catch (Exception $e) {
            dd($e);
            return back()->with('flash_error', 'Promotion Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return Promocode::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $promotion = Promocode::findOrFail($id);
            return view('admin.promotions.edit',compact('promotion'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required',
            'discount' => 'required|numeric',
            'expiration' => 'required',
            'driver_discout_criteria_time' => 'integer|min:0',
            'driver_discout_criteria_distance' => 'integer|min:0',
            'activated_fordriver' => 'integer|in:0,1'
        ]);

        $promotions = Promocode::where('user_type', 'DRIVER')->get();
        if($request->activated_fordriver == '1'){
            $PromotionUsages = PromotionUsages::where('promocode_id', $id)->where('status', '<>', 'USED')->update(['status' => 'ADDED']);
            foreach ($promotions as $promotion) {
                if($promotion->activated_fordriver == '1'){
                    return redirect('admin/promotion')->with('flash_error','One Promotion is already been activated');
                }
            }
        }
        if($request->activated_fordriver == "0"){
            $PromotionUsages = PromotionUsages::where('promocode_id', $id)->where('status', '<>', 'USED')->update(['status' => 'EXPIRED']);
        }
        try {
            Promocode::where('id',$id)->update([
                    'content' => $request->content,
                    'discount' => $request->discount,
                    'expiration' => $request->expiration,
                    'driver_discout_criteria_time' => $request->driver_discout_criteria_time,
                    'driver_discout_criteria_distance' => $request->driver_discout_criteria_distance,
                    'activated_fordriver' => $request->activated_fordriver,
                ]);
            return redirect()->route('admin.promotion.index')->with('flash_success', 'Promotion Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Promotion Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        try {
            Promocode::find($id)->delete();
            return back()->with('message', 'Promotion deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Promotion Not Found');
        }
    }
}
