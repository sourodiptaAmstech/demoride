<?php
namespace App\Http\Controllers\Resource;

use App\WalletPlan;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Exception;
use Setting;

class WalletPlanResource extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $plans = WalletPlan::orderBy('created_at' , 'desc')->get();
        return view('admin.wallet-plan.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.wallet-plan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        try{

            

            
            $WalletPlan = $request->all();
            
            
            //var_dump($WalletPlan);die;


            $WalletPlan = WalletPlan::create($WalletPlan);



            return redirect()->route('admin.wallet-plan.index')->with('flash_success', 'Wallet Plan Successfully'); 

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Cannot Add Wallet Plan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dispatcher  $account
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\wallet-plan  $wallet-plan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try {
            $walletPlan = WalletPlan::findOrFail($id);

           
           // print_r($walletPlan);
            return view('admin.wallet-plan.edit',compact('walletPlan'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\wallet-plan  $wallet-plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $WalletPlan = WalletPlan::findOrFail($id);
           
            $WalletPlan->duration = $request->duration;
            $WalletPlan->amount = $request->amount;
            $WalletPlan->save();

            return redirect()->route('admin.wallet-plan.index')->with('flash_success', 'Wallet Plan Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Wallet Plan Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WalletPlan  $dispatcher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        

        try {
            WalletPlan::find($id)->delete();
            return back()->with('message', 'Wallet Plan deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Wallet Plan Not Found');
        }
    }

}
?>