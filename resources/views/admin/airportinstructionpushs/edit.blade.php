@extends('admin.layout.base')

@section('title', 'Edit Airport Instruction')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
             <a href="{{ route('admin.airportinstructionpush.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Airport Instruction</h5>

            <form class="form-horizontal" action="{{route('admin.airportinstructionpush.update', $airportinstructionpush->id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                
                <div class="form-group row">
                    <label for="title" class="col-xs-12 col-form-label">Title</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $airportinstructionpush->title }}" name="title" required id="title" placeholder="Title">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-form-label">Description</label>
                    <div class="col-xs-10">
                        <textarea rows="6" class="form-control" name="description"  id="description" placeholder="Description">{{ $airportinstructionpush->description }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Update Airport Instruction</button>
                        <a href="{{ route('admin.airportinstructionpush.index') }}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
