@extends('admin.layout.base')

@section('title', 'Provider Wallet History ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Provider Wallet History
                
                @if(Setting::get('demo_mode', 0) == 1)
                <span class="pull-right">(*personal information hidden in demo)</span>
                @endif
            </h5>
            <a href="{{ route('admin.wallet.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Wallet</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Transaction Date</th>
                        <th>Amount</th>
                       <th>Valid From</th>
                        <th>Valid Till</th>
                        <th>Validity Duration</th>
                        <th>Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($getHistory as $index => $wallet)
                    <tr>
                       <td>{{ $index + 1 }}</td>
                       <td>{{ $wallet->created_at }}</td>
                       <td>{{ $wallet->plan->amount }}</td>
                        <td>{{ $wallet->start_date }}</td>
                        <td>{{ $wallet->end_date }}</td>
                        <td>{{ $wallet->duration }}</td>
                        <td>
                           
                                @if($wallet->status == 1)
                                    <label class="btn btn-block btn-primary">Active</label>
                                @else
                                    <label class="btn btn-block btn-warning">Expired</label>
                                @endif
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                    <th>ID</th>
                    <th>Transaction Date</th>
                    <th>Amount</th>
                        <th>Valid From</th>
                        <th>Valid Till</th>
                        <th>Validity Duration</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection