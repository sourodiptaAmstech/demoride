@extends('admin.layout.base')

@section('title', 'Add Location ')

@section('content')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places"></script>
<script>
    function initialize() {
        var input = document.getElementById('location_desc');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('loc_name').value = place.name;
            document.getElementById('loc_Lat').value = place.geometry.location.lat();
            document.getElementById('loc_Long').value = place.geometry.location.lng();
            document.getElementById('loc_place_id').value = place.place_id;
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.locationfee.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Location</h5>

            <form class="form-horizontal" action="{{route('admin.locationfee.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="location_desc" class="col-xs-12 col-form-label">Location</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('location_desc') }}" name="location_desc" required id="location_desc" placeholder="location Name">
                        <input type="hidden" id="loc_name" name="location_name" />
                        <input type="hidden" id="loc_Lat" name="latitude" />
                        <input type="hidden" id="loc_Long" name="longitude" />
                        <input type="hidden" id="loc_place_id" name="place_id" />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fee" class="col-xs-12 col-form-label">Location fee ({{ currency() }})</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('fee') }}" name="fee" required id="fee" placeholder="location fee">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-12 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add location</button>
                        <a href="{{route('admin.locationfee.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
