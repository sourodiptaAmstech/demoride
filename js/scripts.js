$(document).ready(function() {

    var $window = $(window);

    function setCSS() {
        // var windowHeight = $(window).innerHeight();
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        // var logBg = $(".login-right").height();
        // var productImg = $(".product-item-img").outerHeight(true);
        // var productImgBtm = $(".cart-count-block-btm").outerHeight(true);
        // var productImgTop = (productImg - productImgBtm);
        // var cartHead = $(".cart-head").outerHeight(true);
        // var cartFoot = $(".cart-footer").outerHeight(true);
        // var sidebarWrapper = $("#sidebar-wrapper").outerHeight(true);
        // var cartHeadFoot = (cartFoot + cartHead);
        // var cartContent = (sidebarWrapper - cartHeadFoot);

        // var prodpopRight = $(".prod-pop-sec-right").outerHeight(true);


        // $('.log-section').css('min-height', windowHeight);
        // $('.login-left-img').css('min-height', logBg + 'px');
        // $('.cart-count-block-top').css('height', productImgTop + 'px');
        // $('.sidebar').css('height', windowHeight);
        // $('.prod-pop-img').css('height', prodpopRight);
        // $('#sidebar-wrapper').css('height', windowHeight);
        // $('.cart-content').css('height', cartContent);

        $('.slide-box').css('min-height', windowHeight);
    };

    setCSS();
    $(window).on('load resize', function() {
        setCSS();
    });
});

$('.slider-sec').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    fade: true,
    speed: 1200,
    autoplay: true,
    autoplaySpeed: 1200,
    cssEase: 'linear',
    arrows: true
});